export const genres = [
  { _id: "5b21ca3eeb7f6fbccd471818", name: "Rahul" },
  { _id: "5b21ca3eeb7f6fbccd471814", name: "Bharat" },
  { _id: "5b21ca3eeb7f6fbccd471820", name: "Bhushan" }
];

export function getGenres() {
  return genres.filter(g => g);
}
