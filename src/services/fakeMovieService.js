import * as genresAPI from "./fakeGenreService";

const movies = [
  {
    _id: "5b21ca3eeb7f6fbccd471815",
    title: "Test 132",
    brand: "Part two",
    name: "000 88copy of 39533",
    number: "xxxx01",
    task: "task-1",
    deadline: "2018-01-03T19:04:28.809Z",
    genre: { _id: "5b21ca3eeb7f6fbccd471818", name: "Bharat" },
    numberInStock: 6,
    dailyRentalRate: 2.5,
    publishDate: "2018-12-19",
    liked: true
  },
  {
    _id: "5b21ca3eeb7f6fbccd471816",
    title: "TEST 132",
    brand: "Part two",
    name: "000 88copy of 39533",
    number: "xxxx01",
    task: "task-1",
    deadline: "2018-12-19",
    genre: { _id: "5b21ca3eeb7f6fbccd471818", name: "Rahul" },
    numberInStock: 5,
    dailyRentalRate: 2.5
  },
  {
    _id: "5b21ca3eeb7f6fbccd471817",
    title: "TEST Kvadrat",
    brand: "Part two",
    name: "000 88copy of 39533",
    number: "xxxx01",
    task: "task-1",
    deadline: "2018-12-19",
    genre: { _id: "5b21ca3eeb7f6fbccd471820", name: "Bharat" },
    numberInStock: 8,
    dailyRentalRate: 3.5
  },
  {
    _id: "5b21ca3eeb7f6fbccd471819",
    title: "TEST Kvadrat",
    brand: "Part two",
    name: "000 88copy of 39533",
    number: "xxxx01",
    task: "task-1",
    deadline: "2018-12-19",
    genre: { _id: "5b21ca3eeb7f6fbccd471814", name: "Rahul" },
    numberInStock: 7,
    dailyRentalRate: 3.5
  },
  {
    _id: "5b21ca3eeb7f6fbccd47181a",
    title: "TEST Kvadrat",
    brand: "Part two",
    name: "000 88copy of 39533",
    number: "xxxx01",
    task: "task-1",
    deadline: "2018-12-19",
    genre: { _id: "5b21ca3eeb7f6fbccd471814", name: "Bharat" },
    numberInStock: 7,
    dailyRentalRate: 3.5
  },
  {
    _id: "5b21ca3eeb7f6fbccd47181b",
    title: "TEST Kvadrat",
    brand: "Part two",
    name: "000 88copy of 39533",
    number: "xxxx01",
    task: "task-1",
    deadline: "2018-12-19",
    genre: { _id: "5b21ca3eeb7f6fbccd471814", name: "Bhushan" },
    numberInStock: 7,
    dailyRentalRate: 3.5
  },
  {
    _id: "5b21ca3eeb7f6fbccd47181e",
    title: "DBN 08a",
    brand: "Part two",
    name: "000 88copy of 39533",
    number: "xxxx01",
    task: "task-1",
    deadline: "2018-12-19",
    genre: { _id: "5b21ca3eeb7f6fbccd471820", name: "Bhushan" },
    numberInStock: 7,
    dailyRentalRate: 4.5
  },
  {
    _id: "5b21ca3eeb7f6fbccd47181f",
    title: "DBN 08a",
    brand: "Part two",
    name: "000 88copy of 39533",
    number: "xxxx01",
    task: "task-1",
    deadline: "2018-12-19",
    genre: { _id: "5b21ca3eeb7f6fbccd471820", name: "Bharat" },
    numberInStock: 4,
    dailyRentalRate: 3.5
  },
  {
    _id: "5b21ca3eeb7f6fbccd471821",
    title: "TEST 132",
    brand: "Part two",
    name: "000 88copy of 39533",
    number: "xxxx01",
    task: "task-1",
    deadline: "2018-12-19",
    genre: { _id: "5b21ca3eeb7f6fbccd471818", name: "Rahul" },
    numberInStock: 7,
    dailyRentalRate: 3.5
  }
];

export function getMovies() {
  return movies;
}

export function getMovie(id) {
  return movies.find(m => m._id === id);
}

export function saveMovie(movie) {
  let movieInDb = movies.find(m => m._id === movie._id) || {};
  movieInDb.title = movie.title;
  movieInDb.genre = genresAPI.genres.find(g => g._id === movie.genreId);
  movieInDb.numberInStock = movie.numberInStock;
  movieInDb.dailyRentalRate = movie.dailyRentalRate;

  if (!movieInDb._id) {
    movieInDb._id = Date.now().toString();
    movies.push(movieInDb);
  }

  return movieInDb;
}

export function deleteMovie(id) {
  let movieInDb = movies.find(m => m._id === id);
  movies.splice(movies.indexOf(movieInDb), 1);
  return movieInDb;
}
