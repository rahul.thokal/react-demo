import React, { Component } from "react";
import { Link } from "react-router-dom";
import Table from "./common/table";
import Like from "./common/like";
import Form from "./common/form";
import { getMovie, saveMovie } from "../services/fakeMovieService";
import { getGenres } from "../services/fakeGenreService";
import Calendar from 'react-calendar';
import ReactDOM from "react-dom";

import DayPicker from "react-day-picker";
import DayPickerInput from "react-day-picker/DayPickerInput";

class MoviesTable extends Form {
  state = {
    date: new Date(),
    data: { title: "", genreId: "", numberInStock: "", dailyRentalRate: "" },
    genres: [],
    errors: {}
  };
  onChange = date => this.setState({ date })
  componentDidMount = () => {
    const genres = getGenres();
    this.setState({ genres });
  };
  columns = [
    {
      path: "title",
      label: "WORKFLOW",
      content: movie => <Link to={`/todo/${movie._id}`}>{movie.title}</Link>
    },
    { path: "genre.name", label: "BRAND" },
    { path: "name", label: "NAME" },
    { path: "number", label: "NUMBER" },
    { path: "task", label: "TASK" },
    { path: "assigned_to", 
      label: "ASSIGNED TO",
      content: movie => this.renderSelect("genreId", "", this.state.genres) },
    { path: "deadline", label: "DEADLINE",id:"datepicker",
    content: movie => <DayPickerInput placeholder="DD/MM/YYYY" value={movie.deadline} format="DD/MM/YYYY" onClick={() => this.props.handleChange(movie)}  />},
    {
      key: "IN PROGRESS",
      label: "IN PROGRESS",
      content: movie => (
        <Like liked={movie.liked} onClick={() => this.props.onLike(movie)} />
      )
    },
    {
      key: "DONE",
      label: "DONE",
      content: movie => (
        <button
          onClick={() => this.props.onDelete(movie)}
          className="btn btn-primary btn-sm"
        >
          DONE
        </button>
      )
    }
  ];
  render() {
    const { movies, onSort, sortColumn } = this.props;
    return (
      <Table
        columns={this.columns}
        data={movies}
        sortColumn={sortColumn}
        onSort={onSort}
      />
    );
  }
}

export default MoviesTable;
