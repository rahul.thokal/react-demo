import React from "react";

const Like = props => {
  // let classes = "fa fa-heart";
  // if (!props.liked) classes += "-o";
   let classes = "btn btn-";
   let name = "";
  if(!props.liked) {
    classes += "secondary";
    name = "IN PROGRESS"
  }else{
    classes += "success";
    name = "COMPLETED"
  }  
  return (
    <button type="button" 
      onClick={props.onClick}
      style={{ cursor: "pointer" }}
      className={classes}
       aria-hidden="true">{name}</button>
    // <i
    //   onClick={props.onClick}
    //   style={{ cursor: "pointer" }}
    //   className={classes}
    //   aria-hidden="true"
    // />
  );
};
export default Like;
